rtl-433 (25.02-1) unstable; urgency=medium

  * New upstream version.

 -- Alex Myczko <tar@debian.org>  Wed, 19 Feb 2025 16:52:07 +0000

rtl-433 (24.10-1) unstable; urgency=medium

  * New upstream version.

 -- Alex Myczko <tar@debian.org>  Mon, 04 Nov 2024 09:47:52 +0000

rtl-433 (23.11-2) unstable; urgency=medium

  * Move to Hams Team.
  * Bump standards version to 4.7.0.
  * d/control: update build depends pkg-config to pkgconf.

 -- Alex Myczko <tar@debian.org>  Mon, 22 Jul 2024 10:03:05 +0000

rtl-433 (23.11-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: bump years.
  * Bump standards version to 4.6.2.

 -- Gürkan Myczko <tar@debian.org>  Fri, 01 Dec 2023 15:22:43 +0100

rtl-433 (22.11+git20230213+ds-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Mon, 13 Feb 2023 07:33:00 +0100

rtl-433 (22.11-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.6.1.
  * d/control: wrap and sort Build-Depends.

 -- Gürkan Myczko <tar@debian.org>  Mon, 21 Nov 2022 08:24:53 +0100

rtl-433 (21.12+git20220718+ds-2) unstable; urgency=medium

  * Upload to unstable.

 -- Gürkan Myczko <tar@debian.org>  Mon, 25 Jul 2022 09:53:50 +0200

rtl-433 (21.12+git20220718+ds-1) experimental; urgency=medium

  * New upstream version. (Closes: #1008000, #1009788)
    (Addresses CVE-2022-25050, CVE-2022-25051, CVE-2022-27419)

 -- Gürkan Myczko <tar@debian.org>  Mon, 18 Jul 2022 14:36:55 +0200

rtl-433 (21.12-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.6.0.
  * Bump debhelper version to 13, drop d/compat.
  * Update maintainer address.
  * d/copyright: bump copyright years.

  [ Christoph Berg ]
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Gürkan Myczko <tar@debian.org>  Tue, 14 Dec 2021 18:30:32 +0100

rtl-433 (20.02-2) unstable; urgency=medium

  * Add a small patch to improve performance.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 18 Feb 2020 08:10:31 +0100

rtl-433 (20.02-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.5.0.
  * d/rules: force cmake build.
  * d/examples: added.
  * d/control: drop rtl-433-doc package.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 17 Feb 2020 13:08:21 +0100

rtl-433 (19.08-2) unstable; urgency=medium

  * Add Vcs fields.
  * Bump standards version to 4.4.1.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 25 Nov 2019 14:37:51 +0100

rtl-433 (19.08-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 30 Aug 2019 20:32:53 +0200

rtl-433 (18.12+git20190808-2) unstable; urgency=medium

  * Also install config file examples.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 14 Aug 2019 21:05:21 +0200

rtl-433 (18.12+git20190808-1) unstable; urgency=medium

  * Initial release. (Closes: #851309)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 08 Aug 2019 11:28:40 +0200
